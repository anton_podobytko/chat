# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Chat.Repo.insert!(%Chat.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Chat.Repo
alias Chat.User
alias Chat.Room

Repo.get_by(User, username: "anon") || Repo.insert!(%User{username: "anon", password_hash: "xxx"})

Repo.get_by(Room, name: "public") || Repo.insert!(%Room{name: "public", user_ids: [nil]})

