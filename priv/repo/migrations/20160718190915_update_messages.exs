defmodule Chat.Repo.Migrations.UpdateMessages do
  use Ecto.Migration

  def change do
  	alter table(:messages) do
  		modify :inserted_at, :datetime, default: fragment("NOW()")
  	end
  end
end
