defmodule Chat.Repo.Migrations.UpdateMessageTable do
  use Ecto.Migration

  def change do
  	alter table(:messages) do
  		add :at, :string
  	end
  end
end
