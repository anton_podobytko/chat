defmodule Chat.SessionController do
	use Chat.Web, :controller

	def new(conn, _) do
		render conn, "new.html"
	end

	def create(conn, %{"session" => %{"username" => user, "password" => pass}}) do
		case Chat.Auth.login_by_username_and_pass(conn, user, pass, repo: Repo) do
			{:ok, conn} ->
				conn
				|> redirect(to: page_path(conn, :index))
			{:error, _reason, conn} ->
				conn
				|> put_flash(:error, "Invalid username/password")
				|> render("new.html")
		end
	end

	def delete(conn, _) do
		conn
		|> Chat.Auth.logout()
		|> redirect(to: page_path(conn, :index))
	end
end