defmodule Chat.UserController do
	use Chat.Web, :controller
	alias Chat.User

	def new(conn, _params) do
		changeset = User.changeset(%User{})
		render conn, "new.html", changeset: changeset
	end

	def create(conn, %{"user" => user_params}) do
		changeset = User.registration_changeset(%User{}, user_params)

		case Repo.insert(changeset) do
			{:ok, user} ->
				conn
				|> Chat.Auth.login(user)
				|> redirect(to: page_path(conn, :index))
			{:error, changeset} ->
				render(conn, "new.html", changeset: changeset)
		end
	end
end