defmodule Chat.PublicChannel do
	use Chat.Web, :channel
	alias Chat.Repo
	alias Chat.Message
	alias Chat.MessageView
	alias Chat.Presence

	def join("chat:public", _message, socket) do
		room_id = Repo.get_by!(Chat.Room, name: "public").id

		messages = Repo.all(
			from m in Message,
			where: m.room_id == ^room_id,
			order_by: [desc: m.id],
			limit: 15,
			preload: [:user])


		resp = %{messages: Phoenix.View.render_many(messages, MessageView, "messages.json")}
		send(self, :after_join)
		{:ok, resp, assign(socket, :room_id, room_id)}
	end

	def join("chat:" <> _private_room_id, _params, socket) do
		{:error, %{reason: "unauthorized"}}
	end

	def handle_info(:after_join, socket) do
    {:ok, _} = Presence.track(socket, socket.assigns.user.username, %{
      online_at: inspect(System.system_time(:seconds)),
    })
    push socket, "presence_state", Presence.list(socket)
    {:noreply, socket}
  end

	def handle_in("new_msg", params, socket) do
		%{"body" => body, "username" => username, "at" => at} = params
		
		changeset = Message.changeset(%Message{
			body: body, user_id: socket.assigns.user.id, room_id: socket.assigns.room_id, at: at
		})

		case Repo.insert(changeset) do
			{:ok, message} ->
				broadcast! socket, "new_msg", %{body: body, username: username, at: at}
				{:noreply, socket}
			{:error, changeset} ->
				{:reply, {:error, %{errors: changeset}}, socket}
		end
	end

	def handle_in("more_msg", params, socket) do
		first_seen_id = params["first_seen_id"]

		messages = Repo.all(
			from m in Message,
			where: m.room_id == ^socket.assigns.room_id,
			where: m.id < ^first_seen_id,
			order_by: [desc: m.inserted_at],
			limit: 5,
			preload: [:user])

		resp = %{messages: Phoenix.View.render_many(messages, MessageView, "messages.json")}
		broadcast! socket, "more_msg", resp
		{:noreply, socket}
	end

end