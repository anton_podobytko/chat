defmodule Chat.Room do
	use Chat.Web, :model

	schema "rooms" do
		field :name, :string
		field :user_ids, {:array, :integer}

		timestamps
	end
end
