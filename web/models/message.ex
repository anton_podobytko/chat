defmodule Chat.Message do
	use Chat.Web, :model

	schema "messages" do
		field :body, :string
		field :at, :string
		belongs_to :user, Chat.User
		belongs_to :room, Chat.Room
	end


	def changeset(model, params \\ %{}) do
    model
  end
end