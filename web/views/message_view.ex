defmodule Chat.MessageView do
	use Chat.Web, :view

	def render("messages.json", %{message: msg}) do
		%{
			id: msg.id,
			body: msg.body,
			username: msg.user.username,
			at: msg.at
		}
	end
end