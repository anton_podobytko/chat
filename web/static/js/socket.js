import {Socket, Presence} from "phoenix"

let socket = new Socket("/socket", {params: {token: window.userToken}})
socket.connect()

let channel = socket.channel("chat:public", {})
let presences = {}

let msgContainer = $("#chat-box")
let msgInput = $("#message")
let sendButton = $("#send-button")
let currentUsername = $("#current-username")
let moreButton = $("#more")
let usersList = $("#users_list")

msgInput.on("keypress", event => {
	if(event.keyCode === 13){
			sendMessage(channel, msgInput, currentUsername)
		}
})

let listBy = (username, {metas: metas}) => {
  return {
    username: username,
  }
}

let render = (presences) => {
	usersList.html(
		Presence.list(presences, listBy)
			.map(presence => `<li>${presence.username}</li>`)
			.join("")
	)
}

channel.on("presence_state", state => {
	presences = Presence.syncState(presences, state)
	render(presences)
})

channel.on("presence_diff", diff => {
	presences = Presence.syncDiff(presences, diff)
	render(presences)
})

moreButton.on("click", event => {
	channel.push("more_msg", {number: 100, first_seen_id: channel.params.first_seen_id})
})

sendButton.on("click", event => {
	sendMessage(channel, msgInput, currentUsername)
})

channel.on("new_msg", payload => {
	let prepend = false
	renderMessageWithScroll(payload, prepend)
})

channel.on("more_msg", resp => {
	let m_ids = resp.messages.map(msg => msg.id)
	if (m_ids.length > 0) { 
		channel.params.first_seen_id = Math.min(...m_ids)
		resp.messages.forEach(function (msg) {
			renderMessage(msg)
		})
	} else {
		moreButton.addClass("disabled")
		moreButton.fadeOut()
	}
})

msgContainer.on("scroll", event => {
  if (msgContainer.scrollTop() == 0 && !moreButton.hasClass("disabled")) {
    moreButton.fadeIn();
  } else {
    moreButton.fadeOut()
  }
})


channel.join()
  .receive("ok", resp => {
		let m_ids = resp.messages.map(msg => msg.id)
		if (m_ids.length > 0) { channel.params.first_seen_id = Math.min(...m_ids)}

		console.log("Joined successfully")
		resp.messages.forEach(function (msg) {
			renderMessageWithScroll(msg)
		})
  })
  .receive("error", resp => { console.log("Unable to join", resp) })

let renderMessage = (payload, prepend = true) => {
	let template = 
	`
	<div class="item">
    <img src="">
    <p class="message">
      <a href="#" class="name">
      	<small class="text-muted pull-right">
      		<i class="fa fa-clock-o"></i> ${payload.at}
      	</small> 
        ${payload.username}
      </a>
      ${payload.body}
    </p>
  </div>
	`
	if(prepend === true){
		msgContainer.prepend(template)
	} else {
		msgContainer.append(template)
	}
	
}

let renderMessageWithScroll = (payload, prepend) => {
	renderMessage(payload, prepend)
	let height = msgContainer[0].scrollHeight
	msgContainer.scrollTop(height)
}

let sendMessage = () => {
	if(msgInput.val().length > 0){
		let username = (currentUsername.text().length === 0) ? "anon" : currentUsername.text()
		let at = moment().format('MMMM Do YYYY, HH:mm:ss')
		let payload = {body: escapeHtml(msgInput.val()), username: escapeHtml(username), at: at}
		channel.push("new_msg", payload)
		msgInput.val("")
	}
}

let entityMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': '&quot;',
  "'": '&#39;',
  "/": '&#x2F;'
}

let escapeHtml = (string) => {
  return String(string).replace(/[&<>"'\/]/g, function (s) {
    return entityMap[s];
  })
}

export default socket
